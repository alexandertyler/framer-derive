baseLayer = new Layer x:0, y:0, width:650, height:1400, backgroundColor: "black"

city = new Layer x:0, y:0, width:800, height:1400, image:"images/city-scape-1.jpg"
city.states.add
	second: {y:-1400}
	third: {}	
	settings: {x:800}
	mail: {x:1000}
		
title = new Layer x:190, y:20, width:380, height:200, image:"images/derive.png"
title.states.add
	second: {y:-1000}
	third: {}
	settings: {x:800}
	mail: {x:1000}

		
startButton = new Layer x:0, y:200, width:240, height:160, image:"images/GetStarted1.png"
startButton.centerX()
startButton.states.add
	second: {y:-1000}
	third: {}
	settings: {x:1000}
	mail: {x:1000}
	
inbox = new Layer x:0, y:390, width:153, height:153, image:"images/inbox.png"
inbox.centerX()
inbox.states.add
	second: {y:-1000}
	third: {}
	settings: {x:1000}
	mail: {x:1000}
	
mailAccept = new Layer 
	x:0, y:2000, width:620, height:330, image:"images/message.png"
mailAccept.centerX()
mailAccept.states.add
	second: {y:-1000}
	third: {}
	settings: {x:1000}
	mail: {y:300}
	

	
header = new Layer 
	x:0, y:1400, width:750, height:100, backgroundColor: "#2E2E2E"
header.html = 'Derive'
header.style = {
	"textAlign": "center"
	"font-size": "48px" 
	"padding-top": "1cm"}
header.states.add
	second: {y:0}
	third: {}
	settings: {y:0}
	mail: {y:0}
	
settingsButton = new Layer 
	x:0, y:560, width:150, height:150, image:"images/settings.png"
settingsButton.centerX()
settingsButton.states.add
	second: {y:-1000}
	third: {}
	settings: {x:1000}
	mail: {x:1000}
	
settingsBackground = new Layer x:-750, y:100, width:750, height:1300, backgroundColor: "white"
settingsBackground.states.add
	second: {}
	third: {}
	settings: {x:0}
	
	
settingsForms = new Layer x:-750, y:400, width:390, height:220, image:'images/check.png'
settingsForms.states.add
	second: {x:-750}
	settings: {x:175}

optionsBackground = new Layer x:-750, y:100, width:750, height:1300, backgroundColor: "black"
optionsBackground.states.add
	second: {x:0}
	third: {}
	settings: {x:-750}	

optionsHome = new Layer x:10, y:1400, width:80, height:80, image:"images/home.png"
optionsHome.states.add	
	second: {y:10}
	third: {}
	settings: {y:10}
	mail: {y:10}
	
optionsSetting = new Layer x:660, y:1400, width:80, height:80, image:"images/settings.png"
optionsSetting.states.add
	second: {y:10}
	third: {}
	settings: {y:10}
	mail: {y:10}
	
options1 = new Layer x:0, y:1650, width:300, height:180, backgroundColor: "#5882FA", image:"images/cafe.jpg"
options1.centerX()
options1.states.add
	second: {y:250}
	third: {}
	settings: {y:1650}
	
options2 = new Layer x:0, y:1900, width:300, height:180, backgroundColor: "#ED4337", image:"images/tree.png"
options2.centerX()
options2.states.add
	second: {y:500}
	third: {}
	settings: {y:1900}

	
options3 = new Layer x:0, y:2150, width:300, height:180, backgroundColor: "#F0DC78", image:"images/brush.jpg"
options3.centerX()
options3.states.add	
	second: {x:225, y:750}
	third: {x:225, y:250}
	settings: {y:2150}
	friends: {x: -500}
	
options4 = new Layer x:0, y:2400, width:300, height:180, backgroundColor: "#78D278", image:"images/theater.jpg"
options4.centerX()
options4.states.add	
	second: {y:1000}
	third:	{}
	settings: {y:2400}

	
home2options = [city, title, startButton, settingsButton, header, optionsHome, optionsSetting, options1, options2, options3, options4, optionsBackground, settingsForms, inbox, mailAccept]


for state in home2options 
	state.states.animationOptions =
		curve: "linear"
		time: 0.3

	
artBackground = new Layer x:900, y:100, width:800, height:1300, backgroundColor: "#6D7B8D"
artBackground.states.add
	artChoice: {x:0}
	settings: {x:900}
	friends: {x:-900}		
				
		
art1 = new Layer x:900, y:500, width:400, height:200, image:"images/art1.gif"
art1.states.add
	artChoice: {x:175}
	settings: {x:900}
	friends: {x:-400}

art2 = new Layer x:900, y:800, width:400, height:200, image:"images/art2.jpg"
art2.states.add
	artChoice: {x:175}
	settings: {x:900}
	friends: {x:-400}
	
art3 = new Layer x:900, y:1100, width:400, height:200, image:"images/art3.jpg"
art3.states.add
	artChoice: {x:175}
	settings: {x:900}
	friends: {x:-400}
		
artList = [artBackground, art1, art2, art3]


friendsBackground = new Layer x:900, y:100, width:800, height:1300, backgroundColor: "black"
friendsBackground.states.add
	second: {x:900}
	settings: {x:900}
	friends: {x:0}
	features: {x:-800}
	
map1 = new Layer 
	x:800, y:110, width:800, height:400, image:"images/map1.jpg"
map1.states.add
	second: {x:900}
	settings: {x:900}
	friends: {x:0}
	features: {x:-800}
	
friendsList = new Layer x:900, y:520, width:800, height:810, image:"images/friends.png"
friendsList.states.add
	second: {x: 900}
	settings: {x:900}
	friends: {x:0}
	features: {x:-800}
	
featuresList = new Layer x:900, y:200, width:768, height:1152, image: 'images/scenic.png'
featuresList.states.add
	second: {x: 900}
	settings: {x:900}
	friends: {x:900}
	features: {x:0}

toFriends = [artBackground, options3, art1, art2, art3, friendsBackground, map1, friendsList]		

toSettings = [city, startButton, settingsButton, settingsBackground, optionsBackground, artBackground, art1, art2, art3, options1, options2, options3, options4, optionsHome, optionsSetting, header, settingsForms, friendsList, friendsBackground, map1, featuresList, inbox, mailAccept]

toFeatures = [friendsList, friendsBackground, map1, featuresList]

toMail = [city, startButton, inbox, settingsButton, header, optionsHome, optionsSetting, mailAccept]

startButton.on Events.Click, ->
	for state in home2options 
		state.states.animationOptions =
			curve: "linear"
			time: 0.3
		state.states.switch("second")
		
settingsButton.on Events.Click, ->
	for state in toSettings
		state.states.animationOptions = 
			curve: "linear"
			time: 0.3
		state.states.switch("settings")
	
optionsHome.on Events.Click, ->
	for state in home2options
		state.states.animationOptions =
			curve: "linear"
			time: 0.3	
		state.states.switch("second")
	for art in artList
		art.states.switch("default")
	for state in toFriends
		state.states.switch("default")
	for state in toFeatures
		state.states.switch("default")
	options3.states.switch("second")

		
optionsSetting.on Events.Click, ->
	for state in toSettings
		state.states.animationOptions = 
			curve: "linear"
			time: 0.3
		state.states.switch("settings")
	
		
options3.on Events.Click, ->
		options3.states.animationOptions = 
			curve: "spring(100, 10, 5)"
			time: 0.6
		options3.states.switch("third")		
		options3.placeBefore(artBackground)

		for art in artList
			art.states.animationOptions = 
				curve: "ease"
				time: 0.4
			art.states.switch("artChoice")
			
art3.on Events.Click, ->
	for state in toFriends
		state.states.animationOptions = 
			curve: "linear"
			time: 0.3
		state.states.switch("friends")
		
friendsList.on Events.Click, ->
	for state in toFeatures
		state.states.animationOptions = 
			curve: "linear"
			time: 0.3
		state.states.switch("features")
		
inbox.on Events.Click, ->
	for state in toMail
		state.states.switch("mail")
		